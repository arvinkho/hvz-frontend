import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerStatesListItemComponent } from './player-states-list-item.component';

describe('PlayerStatesListItemComponent', () => {
  let component: PlayerStatesListItemComponent;
  let fixture: ComponentFixture<PlayerStatesListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerStatesListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerStatesListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
