import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { GameDto } from 'src/app/models/game-dto';
import { GameCreateDto } from '../models/game-create-dto';
import { GameAPI } from './api/game.api';
import { ErrorService } from './error.service';
import { LoadingService } from './loading.service';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private _allGames: GameDto[] = [];
  private _singleGame!: GameDto;
  private _singleGameSubject: BehaviorSubject<GameDto | null | undefined> = new BehaviorSubject<GameDto | null | undefined>(null);
  private _error: string = "";
  private _loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get allGames() {
    return this._allGames;
  }

  get singleGame() {
    return this._singleGame;
  }

  get singleGame$() {
    return this._singleGameSubject.asObservable();
  }

  get error() {
    return this._error;
  }

  get loading$() {
    return this._loadingSubject.asObservable();
  }


  constructor(private gameAPI: GameAPI, private loadingService: LoadingService,
     private errorService: ErrorService, private toastr: ToastrService) { }
  
  public reset(): void {
    this._singleGameSubject.next(null);
  }

  public getSingleGame(gameId: number): void {
    this.loadingService.startLoading();
    this.gameAPI.getSingleGame(gameId).subscribe({
      next: (game: GameDto) => {
        this._singleGame = game;
        this._singleGameSubject.next(game);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    });
  }

  setSingleGame(game: GameDto): void {
    this._singleGame = game;
  }

  public getAllGames(): void {
    this.loadingService.startLoading();

    this.gameAPI.getAllGames().subscribe({
      next: (games: GameDto[]) => {
        this._allGames = games;
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    });
  }

  public createGame(newGame: GameCreateDto): void {
    this.loadingService.startLoading();
    this.gameAPI.createGame(newGame).subscribe({
      next: (game: GameDto) => {
        this.toastr.success("Successfully created the game " + game.name, "Game creation successful!");
      },
      error: (error:HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    });
  }

  public updateGame(gameId: number, updatedGame: GameCreateDto): void {
    this.loadingService.startLoading();
    this.gameAPI.updateGame(gameId, updatedGame).subscribe({
      next: (game: GameDto) => {
        this.toastr.success("Successfully updated the game " + game.name, "Game update successful!");
      },
      error: (error:HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 404) {
          this.toastr.error("Game not found.", "Game update failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public deleteGame(gameId: number) {
    this.loadingService.startLoading();
    this.gameAPI.deleteGame(gameId).subscribe({
      error: (error:HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 404) {
          this.toastr.error("Game not found.", "Game deletion failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
        this.toastr.success("Successfully deleted the game", "Game deletion successful!");
      }
    })
  }

}
