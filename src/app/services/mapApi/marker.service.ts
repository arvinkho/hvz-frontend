import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as L from 'leaflet';
import { PopupService } from './popup.service';
import { KillService } from '../kill.service';
import { MissionService } from '../mission.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { KillDto } from 'src/app/models/kill-dto';


@Injectable({
  providedIn: 'root'
})
export class MarkerService {

  markerImages: string = 'assets/images/hvz_markers/';
  private _coords: BehaviorSubject<L.LatLng[]> = new BehaviorSubject<L.LatLng[]>([])
  
  private _killMarkers: BehaviorSubject<L.Marker[]> = new BehaviorSubject<L.Marker[]>([])
  private _kills: KillDto[] = [];

  

  constructor(
    private http: HttpClient,
    private popupService: PopupService,
    private missionService: MissionService,
    private killService: KillService
    ) { }

  makeLeafMarker(url: string): any {

    var LeafIcon = L.Icon.extend({
      options: {
        iconUrl: url ,
        shadowUrl: 'assets/images/leaf-shadow.png',
          iconSize:     [38, 95],
          shadowSize:   [50, 64],
          iconAnchor:   [22, 94],
          shadowAnchor: [4, 62],
          popupAnchor:  [-3, -76]
      }
    });

    return new LeafIcon
  }

  makeHVZMarker(url: string): any {
    var HVZmarker = L.Icon.extend({
      options: {
        iconUrl: url,
        shadowUrl: 'assets/marker-shadow.png',
        iconSize:     [30, 35],
          shadowSize:   [50, 64],
          iconAnchor:   [12, 36],
          shadowAnchor: [4, 62],
          popupAnchor:  [-3, -36]
      }
    });
    return new HVZmarker;
  }

  createKillMarker(){
    var markerUrl: string = 'gravestone.png';
    return this.makeHVZMarker(this.markerImages+markerUrl);
  }

  createMissionMarker(){
    var markerUrl: string = 'mission.png';
    return this.makeHVZMarker(this.markerImages+markerUrl);
  }

  createZombieMarker(){
    var markerUrl: string = 'zombie.png';
    return this.makeHVZMarker(this.markerImages+markerUrl);
  }

  createSquadMarker(){
    var markerUrl: string = 'squad.png';
    return this.makeHVZMarker(this.markerImages+markerUrl);
  }

  createPlayerMarker(){
    var markerUrl: string = 'player.png';
    return this.makeHVZMarker(this.markerImages+markerUrl);
  }

  createMarker(){
    var markerUrl: string = 'marker.png';
    return this.makeHVZMarker(this.markerImages+markerUrl);
  }

  createDeathMarker(){
    var markerUrl: string = 'death.png';
    return this.makeHVZMarker(this.markerImages+markerUrl);
  }

  setDeathMarker(map: L.Map, coords:L.LatLng){
    var markerUrl: string = 'death.png';
    var deathMarker = this.makeHVZMarker(this.markerImages+markerUrl);
    L.marker(coords, {icon: deathMarker}).addTo(map);
  }


  createPolygon(coord1: L.LatLngExpression[], coord2: L.LatLngExpression[], coord3: L.LatLngExpression[]){
    var polygon = L.polygon([
      coord1,
      coord2,
      coord3
  ]);
  return polygon;
  }

  createCirle(latitude: number, longitude: number){
    var circle = L.circle([latitude, longitude], {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.5,
      radius: 500
  })

    return circle;
  }

  createRectangle(latLng1: L.LatLng, latLng2: L.LatLng){
    var bounds = L.latLngBounds(latLng1, latLng2);
    var rectOptions = {color: 'Red', weight: 1}
    var rectangle = L.rectangle(bounds, rectOptions);
    return rectangle;
  }

  getAllKills(gameId: number){
    return this.killService.getAllKills$(gameId)
  }

  getAllMissions(gameId: number){
    return this.missionService.getAllMissions$(gameId);
  }
}
