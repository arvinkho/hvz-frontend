export class GameDto {
    gameId: number;
    name: string;
    gameState: string; //game state enum, REGISTRATION, INPROGRESS, ENDED
    description: string;
    rules: string;
    nwLatitude: number;
    nwLongitude: number;
    seLatitude: number;
    seLongitude: number;
    missionIds: number[];
    playerIds: number[];
    killIds: number[];
    squadIds: number[];
    squadMemberIds: number[];
    squadCheckinIds: number[];
    playerCount: number;

    constructor(gameId: number, name: string, gameState: string, description: string, rules: string, nwLatitude: number, nwLongitude: number, seLatitude: number, seLongitude: number,
        missionIds: number[], playerIds: number[], killIds: number[], squadIds: number[], squadMemberIds: number[], squadCheckinIds: number[], playerCount: number) {
        this.gameId = gameId;
        this.name = name;
        this.gameState = gameState;
        this.description = description;
        this.rules = rules;
        this.nwLatitude = nwLatitude;
        this.nwLongitude = nwLongitude;
        this.seLatitude = seLatitude;
        this.seLongitude = seLongitude;
        this.missionIds = missionIds;
        this.playerIds = playerIds;
        this.killIds = killIds;
        this.squadIds = squadIds;
        this.squadMemberIds = squadMemberIds;
        this.squadCheckinIds = squadCheckinIds;
        this.playerCount = playerCount;
    }

}