import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { MissionCreateDto } from "src/app/models/mission-create-dto";
import { MissionDto } from "src/app/models/mission-dto";
import { environment as env } from "src/environments/environment";

const API_URL = env.dev.serverUrl + env.dev.endpoints.gameApi;

@Injectable({
    providedIn: "root"
})
// DO NOT INJECT, ONLY USED FOR MISSION SERVICE

export class MissionAPI {
    constructor(private http: HttpClient) {}

    public getAllMissions(gameId: number): Observable<MissionDto[]> {
        return this.http.get<MissionDto[]>(API_URL + "/" + gameId + "/mission");
    }

    public getSingleMission(gameId: number, missionId: number): Observable<MissionDto> {
        return this.http.get<MissionDto>(API_URL + "/" + gameId + "/mission/" + missionId);
    }

    public createMission(gameId: number, newMission: MissionCreateDto): Observable<MissionDto> {
        return this.http.post<MissionDto>(API_URL + "/" + gameId + "/mission", newMission);
    }

    public updateMission(gameId: number, missionId: number, newMission: MissionCreateDto): Observable<MissionDto> {
        return this.http.put<MissionDto>(API_URL + "/" + gameId + "/mission/" + missionId, newMission);
    }

    public deleteMission(gameId: number, missionId: number): Observable<unknown> {
        return this.http.delete(API_URL + "/" + gameId + "/mission/" + missionId);
    }
}