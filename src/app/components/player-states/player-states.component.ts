import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { PlayerDto } from "src/app/models/player-dto";
import { GameService } from "src/app/services/game.service";
import { PlayerService } from "src/app/services/player.service";

@Component({
	selector: "app-player-states",
	templateUrl: "./player-states.component.html",
	styleUrls: ["./player-states.component.css"],
})
export class PlayerStatesComponent implements OnInit {
	get players(): PlayerDto[] {
		return this.playerService.allPlayersInGame;
	}

	chooseGame: FormGroup;

	//DI
	constructor(private readonly playerService: PlayerService,
		public gameService: GameService) {
			this.chooseGame = new FormGroup({
				gameId: new FormControl("")
			})
		}

	ngOnInit(): void {
		this.gameService.getAllGames();
	}

	onChooseGameSubmit(){
		this.playerService.getAllPlayersInGame(this.chooseGame.value.gameId);

	}
}
