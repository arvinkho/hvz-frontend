import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { MissionCreateDto } from '../models/mission-create-dto';
import { MissionDto } from '../models/mission-dto';
import { MissionAPI } from './api/mission.api';
import { ErrorService } from './error.service';
import { LoadingService } from './loading.service';

@Injectable({
  providedIn: 'root'
})
export class MissionService {

  private _allMissions: MissionDto[] = [];
  private _singleMission!: MissionDto;
  private _error: string = "";
  private _loading: boolean = false;

  get allMissions() {
    return this._allMissions;
  }

  get singleMission() {
    return this._singleMission;
  }

  get error() {
    return this._error;
  }

  get loading() {
    return this._loading;
  }

  constructor(private missionAPI: MissionAPI, private loadingService: LoadingService, private errorService: ErrorService, private toastr: ToastrService) { }

  public getSingleMission(gameId: number, missionId: number): void {
    this.loadingService.startLoading();
    this.missionAPI.getSingleMission(gameId, missionId).subscribe({
      next: (mission: MissionDto) => {
        this._singleMission = mission;
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public getAllMissions$(gameId:number): Observable<MissionDto[]>{
      return this.missionAPI.getAllMissions(gameId);
  }

  public getAllMissions(gameId: number): void {
    this.loadingService.startLoading();
    this.missionAPI.getAllMissions(gameId).subscribe({
      next: (missions: MissionDto[]) => {
        this._allMissions = missions;
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public createMission(gameId: number, newMission: MissionCreateDto): void {
    this.loadingService.startLoading();
    this.missionAPI.createMission(gameId, newMission).subscribe({
      next: (mission: MissionDto) => {
        this._singleMission = mission;
        this.toastr.success("You successfully created the mission: " + this._singleMission.name, "Mission created!");
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        if (error.status === 404) {
          this.toastr.error("Game not found or mission is already created!", "Creating mission failed")
        }
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public updateMission(gameId: number, missionId: number, newMission: MissionCreateDto): void {
    this.loadingService.startLoading();
    this.missionAPI.updateMission(gameId, missionId, newMission).subscribe({
      next: (mission: MissionDto) => {
        this._singleMission = mission;
        this.toastr.success("You successfully updated the mission: " + this._singleMission.name, "Mission updated!");
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        if (error.status === 400) {
          this.toastr.error("Mission or game does not exist", "Updating mission failed")
        }
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public deleteMission(gameId: number, missionId: number): void {
    this.loadingService.startLoading();
    this.missionAPI.deleteMission(gameId, missionId).subscribe({
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 404) {
          this.toastr.error("Mission not found.", "Mission deletion failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
        this.toastr.success("Successfully deleted the mission", "Mission deletion successful!");
      }
    })
  }

}
