
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { GameDto } from 'src/app/models/game-dto';
import { SquadDto } from 'src/app/models/squad-dto';
import { PlayerService } from 'src/app/services/player.service';
import { SquadService } from 'src/app/services/squad.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-squad-list-item',
  templateUrl: './squad-list-item.component.html',
  styleUrls: ['./squad-list-item.component.css']
})
export class SquadListItemComponent implements OnInit {

  public currentWindowWidth: number;

  @Input() _squad?: SquadDto;
  @Input() _game?: GameDto; 

  constructor(
    public userService: UserService,
    private squadService: SquadService
  ) { }

  ngOnInit(): void {
    this.currentWindowWidth = window.innerWidth;
  }

  public onJoinClick(): void {
    this.userService.joinSquad(this._squad!.squadId);
    //this.squadService.getAllSquads(this._game!.gameId);
  }

  public onLeaveSquad(): void {
    this.userService.leaveSquad();
    this.squadService.getAllSquads(this._game!.gameId);
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth
  }

}
