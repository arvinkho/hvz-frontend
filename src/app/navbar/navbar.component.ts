import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlayerDto } from '../models/player-dto';
import { UserDto } from '../models/user-dto';
import { PlayerService } from '../services/player.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public currentWindowWidth: number;

  constructor(
    private router: Router,
    public userService: UserService,
    private playerService: PlayerService,
  ) { 

  }

  ngOnInit(): void {
    this.currentWindowWidth = window.innerWidth;
  }

  public toAdmin(): void {
    this.router.navigate(["/admin"]);
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth
  }

}
