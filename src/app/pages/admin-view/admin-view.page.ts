import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { PlayerDto } from 'src/app/models/player-dto';
import { PlayerService } from 'src/app/services/player.service';

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.page.html',
  styleUrls: ['./admin-view.page.css']
})
export class AdminViewPage implements OnInit {
  
  constructor(
    public authService: AuthService
  ) { }


  ngOnInit(): void {    
  }

}
