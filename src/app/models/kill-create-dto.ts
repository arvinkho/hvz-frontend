export class KillCreateDto {
    story?: string;
    latitude?: number;
    longitude?: number;
    biteCode: string;
    gameId: number;
    killerId: number;

    constructor(biteCode: string, killerId: number, gameId: number, story?: string, latitude?: number, longitude?: number, 
         ) {
        this.biteCode = biteCode;
        this.gameId = gameId;
        this.killerId = killerId;
        this.story = story;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}