import { Observable } from "rxjs";
import { KillDto } from "./kill-dto";

export class UserModel{

    name?: string;
    email?: string;
    avatarUrl?: string;
    userId?: string;
    playerId?: number | null;
    isHuman?: boolean;
    isPatientZero?: boolean;
    gameId?: number | null;
    kills: KillDto[] = [];

    

    constructor() {}

    
    public setName(name: string): void {
        this.name = name;
    }

    public setEmail(email: string): void {
        this.email = email;
    }

    public setAvatarUrl(avatarUrl: string): void {
        this.avatarUrl = avatarUrl;
    }

    public setUserId(userId: string): void {
        this.userId = userId;
    }

    public setPlayerId(playerId: number | null): void {
        this.playerId = playerId;
    }

    public setIsHuman(isHuman: boolean): void {
        this.isHuman = isHuman;
    }

    public setIsPatientZero(isPatientZero: boolean): void {
        this.isPatientZero = isPatientZero;
    }

    public setGameId(gameId: number | null): void {
        this.gameId = gameId;
    }

    public addKill(kill: KillDto): void {
        this.kills.push(kill);
    }
    

    // constructor(name: string, email: string, userId: string, isHuman: boolean, isPatientZero: boolean, gameId: number) {
    //     this.name = name;
    //     this.email = email;
    //     this.userId = userId;
    //     this.isHuman = isHuman;
    //     this.isPatientZero = isPatientZero;
    //     this.gameId = gameId;
    // }
}