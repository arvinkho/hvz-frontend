import { Component } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { UserModel } from 'src/app/models/user-model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: "./user-profile.component.html",
})
export class UserProfileComponent{
  public profileJson: string | null = null;

  constructor(public auth: AuthService, public userService: UserService) {
    
  }
}
