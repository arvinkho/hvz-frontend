import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { KillCreateDto } from '../models/kill-create-dto';
import { KillDto } from '../models/kill-dto';
import { KillAPI } from './api/kill.api';
import { ErrorService } from './error.service';
import { LoadingService } from './loading.service';

@Injectable({
  providedIn: 'root'
})
export class KillService {

  private _allKills: KillDto[] = [];
  private _allKillsSubject: BehaviorSubject<KillDto[]> = new BehaviorSubject<KillDto[]>([]);
  private _singleKill!: KillDto;
  private _lastKill: KillDto | null | undefined;

  get allKills() {
    return this._allKillsSubject.getValue();
  }

  get allKills$(): Observable<KillDto[]> {
    return this._allKillsSubject.asObservable();
  }

  get singleKill() {
    return this._singleKill;
  }

  get lastKill() {
    return this._lastKill;
  }

  constructor(private killAPI: KillAPI, private loadingService: LoadingService, private errorService: ErrorService, private toastr: ToastrService) { }

  public getSingleKill(gameId: number, killId: number): void {
    this.loadingService.startLoading();
    this.killAPI.getSingleKill(gameId, killId).subscribe({
      next: (kill: KillDto) => {
        this._singleKill = kill;
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public getAllKills(gameId: number): void {
    this.loadingService.startLoading();
    this.killAPI.getAllKillsInGame(gameId).subscribe({
      next: (kills: KillDto[]) => {
        this._allKillsSubject.next(kills);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  /** 
   * Returns HTTP Observable to be used for polling in mapComponent.
   * Purpose is to always get the updated list of kills.
   */
  public getAllKills$(gameId: number): Observable<KillDto[]> {
    return this.killAPI.getAllKillsInGame(gameId)
  }


  public createKill(gameId: number, newKill: KillCreateDto): void {
    this.loadingService.startLoading();
    this.killAPI.createKill(gameId, newKill).subscribe({
      next: (kill: KillDto) => {
        this._lastKill = kill;
        this.toastr.success("You successfully killed " + this._lastKill.victimName, "Kill successful!", {
          timeOut: 7500
        });
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        if (error.status === 400) {
          this.toastr.error("Bite code is invalid or victim is already dead!", "Kill failed", {
            timeOut: 7500
          })
        }
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
        this.getAllKills(gameId);
      }
    })
  }

  public updateKill(gameId: number, killId: number, newKill: KillCreateDto): void {
    this.loadingService.startLoading();
    this.killAPI.updateKill(gameId, killId, newKill).subscribe({
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public deleteKill(gameId: number, killId: number): void {
    this.loadingService.startLoading();
    this.killAPI.deleteKill(gameId, killId).subscribe({
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }


}
