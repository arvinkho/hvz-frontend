export class PlayerCreateDto {
    human: boolean;
    name: string;
    userId: string;
    gameId: number;

    constructor(human: boolean, name: string, userId: string, gameId: number) {
        this.human = human;
        this.name = name;
        this.userId = userId;
        this.gameId = gameId;
    }
}