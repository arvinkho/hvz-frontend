export class SquadCheckinDto {
    latitude: number;
    longitude: number;
    gameId: number;
    squadId: number;
    squadMemberId: number;
    squadCheckinId: number;
    

    constructor(latitude: number, longitude: number, gameId: number, squadId: number, squadMemberId: number, squadCheckinId: number) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.gameId = gameId;
        this.squadId = squadId;
        this.squadMemberId = squadMemberId;
        this.squadCheckinId = squadCheckinId;
    }
}