import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as L from "leaflet"
import { MapService } from '../services/mapApi/map.service';


let Stadia_OSMBright = L.tileLayer('https://tiles.stadiamaps.com/tiles/osm_bright/{z}/{x}/{y}{r}.png', {
	maxZoom: 20,
	attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
});

@Component({
  selector: 'app-leaflet-map',
  templateUrl: './leaflet-map.component.html',
  styleUrls: ['./leaflet-map.component.css']
})


export class LeafletMapComponent implements OnInit, AfterViewInit{

  options: L.MapOptions
  centroid: L.LatLng  
  layersControl: any;

  layers : {
    'zones' : L.Layer,
    'players': L.Layer,
    'kills': L.Layer,
    'missions': L.Layer
  }


  constructor(private mapService: MapService) { }

  ngOnInit(): void {
    
    this.mapService.getCurrentPosition().then(pos => {
      this.centroid = L.latLng(pos.lat, pos.lng)
      console.log(this.centroid);
    })

    this.options = {
      layers: [
        Stadia_OSMBright
      ], 
      zoom: 13,
      center: L.latLng(59.9228416, 10.747904)
    }

   this.layersControl = {
      baseLayers: {
        'Open Street Map': L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
        'Open Cycle Map': L.tileLayer('http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
        'Deafult Map': Stadia_OSMBright
      },
      overlays: {
        'Big Circle': L.circle([ 46.95, -122 ], { radius: 5000 }),
        'Big Square': L.polygon([[ 46.8, -121.55 ], [ 46.9, -121.55 ], [ 46.9, -121.7 ], [ 46.8, -121.7 ]])
      }
    }


    
  }
  ngAfterViewInit(): void {
  }

  onMapReady(leafletMap:L.Map){
    console.log("SHITS HERE DUMBAASS");
    setTimeout(() => {leafletMap.invalidateSize(true)}, 1000);
    // map.invalidateSize(true);
  }
}
