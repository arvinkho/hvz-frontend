import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { takeWhile } from "rxjs";
import { GameCreateDto } from "src/app/models/game-create-dto";
import { GameDto } from "src/app/models/game-dto";
import { GameService } from "src/app/services/game.service";
import { LoadingService } from "src/app/services/loading.service";

@Component({
	selector: "app-admin-game",
	templateUrl: "./admin-game.component.html",
	styleUrls: ["./admin-game.component.css"],
})
export class AdminGameComponent implements OnInit {
	createGameForm: FormGroup;
	editGameForm: FormGroup;
	gameIdForm: FormGroup;

	get singleGame(): GameDto {
		return this.gameService.singleGame;
	}

	constructor(public gameService: GameService,
		private loadingService: LoadingService) {
		this.createGameForm = new FormGroup({
			inputGameName: new FormControl(""),
			inputGameState: new FormControl(""),
			inputGameDescription: new FormControl(""),
			inputNwLat: new FormControl(""),
			inputNwLong: new FormControl(""),
			inputSeLat: new FormControl(""),
			inputSeLong: new FormControl(""),
		});

		this.gameIdForm = new FormGroup({
			gameId: new FormControl(""),
		});

		this.editGameForm = new FormGroup({
			gameName: new FormControl(""),
			gameState: new FormControl(""),
			gameDescription: new FormControl(""),
			nwLat: new FormControl(""),
			nwLong: new FormControl(""),
			seLat: new FormControl(""),
			seLong: new FormControl(""),
		});
	}

	ngOnInit(): void {
		this.gameService.getAllGames();
	}

	onSubmit(): void {
		const newGame: GameCreateDto = new GameCreateDto(
			this.createGameForm.value.inputGameName,
			this.createGameForm.value.inputGameDescription,
			this.createGameForm.value.inputNwLat,
			this.createGameForm.value.inputNwLong,
			this.createGameForm.value.inputSeLat,
			this.createGameForm.value.inputSeLong,
			"0"
		);

		this.gameService.createGame(newGame);

		this.createGameForm.reset();
	}

	onGameIdSubmit(): void {
		const gameId: number = this.gameIdForm.value.gameId;
		this.gameService.getSingleGame(gameId);
		this.loadingService.loading$.pipe(takeWhile( loading => this.loadingService.loading == true))
		.subscribe({
			complete: () => {
				if (gameId === null) {
					this.editGameForm.patchValue({
						gameName: (""),
						gameState: (""),
						gameDescription: (""),
						nwLat: (""),
						nwLong: (""),
						seLat: (""),
						seLong:(""),
					});
				} else {
					this.editGameForm.patchValue({
						gameName: this.singleGame.name,
						gameState: this.singleGame.gameState,
						gameDescription: this.singleGame.description,
						nwLat: this.singleGame.nwLatitude,
						nwLong: this.singleGame.nwLongitude,
						seLat: this.singleGame.seLatitude,
						seLong: this.singleGame.seLongitude,
					})
				}
			  }
		})

	}

	onEdit(): void {
		const updateGame: GameCreateDto = new GameCreateDto(
			this.editGameForm.value.gameName,
			this.editGameForm.value.gameDescription,
			this.editGameForm.value.nwLat,
			this.editGameForm.value.nwLong,
			this.editGameForm.value.seLat,
			this.editGameForm.value.seLong,
			this.editGameForm.value.gameState
		);

		this.gameService.updateGame(this.gameIdForm.value.gameId, updateGame);

		console.log("Game ID: " + this.gameIdForm.value.gameId);
		console.log("Updated game" + this.editGameForm.value);
	}

	onClose(): void {
		const gameIdCheck = this.gameIdForm.value;
		if (gameIdCheck === null || undefined) {
			this.editGameForm.reset();
		}
	}
}
