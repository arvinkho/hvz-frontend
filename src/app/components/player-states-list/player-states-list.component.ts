import { Component, Input, OnInit } from '@angular/core';
import { PlayerDto } from 'src/app/models/player-dto';

@Component({
  selector: 'app-player-states-list',
  templateUrl: './player-states-list.component.html',
  styleUrls: ['./player-states-list.component.css']
})
export class PlayerStatesListComponent implements OnInit {
  
  //Allows to pass input into the component
  @Input() playerList: PlayerDto[] = [];
  
  constructor() { }

  ngOnInit(): void {
  }

}
