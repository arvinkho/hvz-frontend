export class PlayerDto {
    playerId: number;
    name: string;
    human: boolean;
    biteCode: string;
    userId: number;
    gameId: number;
    squadMemberId: number;

    constructor(playerId: number, name: string,  human: boolean, biteCode: string, userId: number, gameId: number, squadMemberId: number) {
        this.playerId = playerId;
        this.name = name;
        this.human = human;
        this.biteCode = biteCode;
        this.userId = userId;
        this.gameId = gameId;
        this.squadMemberId = squadMemberId;
    }
}