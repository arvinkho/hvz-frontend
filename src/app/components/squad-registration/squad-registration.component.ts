import { Component, HostListener, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GameDto } from 'src/app/models/game-dto';
import { SquadDto } from 'src/app/models/squad-dto';
import { SquadService } from 'src/app/services/squad.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-squad-registration',
  templateUrl: './squad-registration.component.html',
  styleUrls: ['./squad-registration.component.css']
})
export class SquadRegistrationComponent implements OnInit {

  public currentWindowWidth: number;

  @Input() game?: GameDto;

  constructor(
    public squadService: SquadService,
    public userService: UserService,
  ) { }

  ngOnInit(): void {
    this.squadService.getAllSquads(this.game!.gameId);
    this.currentWindowWidth = window.innerWidth;
  }

  public onSubmitNewSquad(registerSquad: NgForm): void {
    const squadName: string = registerSquad.value.squadName;
    this.userService.createSquad(squadName);
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth
  }

}
