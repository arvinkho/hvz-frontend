import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, take } from 'rxjs';
import { GameDto } from 'src/app/models/game-dto';
import { UserModel } from 'src/app/models/user-model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.css']
})
export class GameDetailsComponent implements OnInit {
  @Input() game?: GameDto;

  public currentWindowWidth: number;

  inProgress: boolean = false;

  constructor(
    private router: Router,
    public userService: UserService
  ) {}

  ngOnInit(): void {
    if (this.game === undefined) { 
      this.router.navigateByUrl("/"); 
    };

    if (this.game?.gameState !== "REGISTRATION") {
      this.inProgress = true;
    }
    else {
      this.inProgress = false;
    }

    this.currentWindowWidth = window.innerWidth;

  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth
  }

  onRegisterClick(): void {
    this.userService.setGame(this.game?.gameId!);
  }

  onLeaveGameClick(): void {
    this.userService.leaveGame();
  }
}
