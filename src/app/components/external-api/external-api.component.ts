import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { GameCreateDto } from 'src/app/models/game-create-dto';
import { GameDto } from 'src/app/models/game-dto';
import { GameService } from 'src/app/services/game.service';
import { PlayerService } from 'src/app/services/player.service';
import { UserService } from 'src/app/services/user.service';
import { environment as env } from 'src/environments/environment';

interface Message{
  title: string;
}

@Component({
  selector: 'app-external-api',
  templateUrl: './external-api.component.html',
  styleUrls: ['./external-api.component.css']
})



export class ExternalApiComponent implements OnInit {
  public message?: string = "";
  public games?: GameDto[];
  public singleGame?: GameDto;
  
  get game$() {
    return this.userService.game$;
  }

  get player$() {
    return this.userService.player$;
  }

  constructor(public game: GameService, public playersService: PlayerService, public userService: UserService) { }

  ngOnInit(): void {
    
  }

  getAllPlayers(): void {
    this.playersService.getAllPlayers();
  }

  getAllPlayersInSquad(): void {
    this.playersService.getAllPlayersInSquad(1000, 1000);
  }

  getGames(): void {
    this.game.getAllGames();
  }

  getSingleGame(gameId: number): void {
    this.userService.setGame(gameId);
  }

  createSquad(name: string): void {
    this.userService.createSquad(name);
  }

  joinSquad(squadId: number): void {
    this.userService.joinSquad(squadId);
  }

  leaveSquad(): void {
    this.userService.leaveSquad();
  }

  setPlayer(): void {
    this.userService.setPlayer();
  }

  leaveGame(): void {
    this.userService.leaveGame();
  }

  createGame(): void {
    const newGame: GameCreateDto = new GameCreateDto("Testgame3", "This game is about zombies vs humans", 30, 40, 30, 40, "REGISTRATION");
    this.game.createGame(newGame);
  }

  updateGame(): void {
    const newGame: GameCreateDto = new GameCreateDto("UpdatedGame", "You need to kill the zombies", 30, 40, 30, 40, "IN_PROGRESS");
    
    this.game.updateGame(1, newGame);
  }

  deleteGame(gameId: number): void {
    this.game.deleteGame(gameId);
  }

}
