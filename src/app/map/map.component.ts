import { Component, AfterViewInit, OnInit, OnDestroy, Input } from '@angular/core';
import * as L from 'leaflet';

import { BehaviorSubject, map, Subscription } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { interval } from 'rxjs';

import { MapService } from '../services/mapApi/map.service';
import { MarkerService } from '../services/mapApi/marker.service';
import { GameDto } from '../models/game-dto';
import {Router } from "@angular/router"
import { GameService } from '../services/game.service';

const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit, AfterViewInit, OnDestroy {

  private map: L.Map;
  private centroid : L.LatLng;
  private _graveStones: BehaviorSubject<L.Marker[]> = new BehaviorSubject<L.Marker[]>([]);
  private killMarkers: L.LayerGroup = new L.LayerGroup;
  private markerLayer: L.LayerGroup;

  private game: GameDto

  timeInterval:  Subscription;
  status: any;


  get gravestones$(){
    return this._graveStones.asObservable();
  }


  constructor(
    private markerService: MarkerService,
    private mapService: MapService,
    private router: Router,
    private gameService: GameService
    ) {  }


  private createMap(){
      /*
      this.mapService.getCurrentPosition().then(pos => {
        this.centroid = L.latLng(pos.lat, pos.lng)
      })
      */
      this.map = L.map('map', {
        center: L.latLng(59.9228416, 10.747904), 
        zoom:12
      });

      const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      minZoom: 2,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);

  }

  private initializeMarkers(){
    var gravestone = this.markerService.createKillMarker();
    var missionMarker = this.markerService.createMissionMarker();

    L.marker([59.916796,10.767507], {icon: missionMarker}).addTo(this.map).bindPopup;

    L.marker([59.917983,10.769383], {icon: gravestone}).addTo(this.map).bindPopup
      ('<h2>In loving memory of</h2> <img src="assets/images/simen.jpg" width="100" height="150"/>');
   

  var latLng1: L.LatLng = L.latLng(59.916314, 10.762079);
  var latLng2: L.LatLng = L.latLng(59.919820,10.774661);
  this.markerService.createRectangle(latLng1, latLng2).addTo(this.map)

  var playerIcon = this.markerService.createPlayerMarker();
  L.marker(this.mapService.getPosition(), {icon: playerIcon}).addTo(this.map).bindPopup('This is you!')

  }


  ngOnInit(): void {
      this.centroid = this.mapService.getPosition();
      console.log(this.mapService.getPosition());

      this.game = this.gameService.singleGame;
      if(!this.game){
        this.router.navigateByUrl('')
      }
  }


  ngAfterViewInit(): void {
      this.createMap();
      this.markerLayer = new L.LayerGroup();
      this.markerLayer.addTo(this.map)
      this.initializeMarkers();
      var gravestone = this.markerService.createKillMarker()

      this.timeInterval = interval(60000)
      .pipe(
        startWith(0),
        switchMap(() => this.markerService.getAllKills(this.game.gameId)))
        .subscribe({
          next:(kills) => {
            if (kills) {
              this.markerLayer.clearLayers();
              for(let kill of kills){
               const marker = L.marker([kill.latitude, kill.longitude], {icon: gravestone})
                marker.addTo(this.markerLayer).bindPopup(`
                <ul class="list-unstyled">
                  <li><b>${kill.killerName} killed ${kill.victimName}</b></li>
                  <li>Story: ${kill.story}</li>
                  <li>Time of death: ${new Date(kill.timeOfDeath).toUTCString()}</li>
                </ul>
                `);
              }
            }
            
          }, 
          error: (error) => {
            console.log(error);
          }
        });

}

  ngOnDestroy(): void {
      this.timeInterval.unsubscribe();
  }

  onClick(){
    this.router.navigateByUrl(`/game-detail/${this.game!.gameId}`)
  }

  setMarkerOnMap(marker: L.Marker){
    marker.addTo(this.map)
  }

  addLayer(layer: L.Layer){
    layer.addTo(this.map);
  }

  deleteMarkers(){
  }

  deleteLayer(layer: L.Layer){
      layer.removeFrom(this.map)
  }

  deleteMarker(marker: L.Marker){
     marker.removeFrom(this.map);
  }

  checkIfMarkerExists(marker: L.Marker): boolean{
    return this.killMarkers.hasLayer(marker)
  }
}