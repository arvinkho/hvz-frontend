import { Component, HostListener, Input, OnInit } from '@angular/core';
import { PlayerDto } from 'src/app/models/player-dto';
import { SquadMemberDto } from 'src/app/models/squad-member-dto';

@Component({
  selector: 'app-squad-member-list',
  templateUrl: './squad-member-list.component.html',
  styleUrls: ['./squad-member-list.component.css']
})
export class SquadMemberListComponent implements OnInit {

  public currentWindowWidth: number;

  @Input() _squadMember: PlayerDto;

  constructor() { }

  ngOnInit(): void {
    this.currentWindowWidth = window.innerWidth;
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth
  }

}
