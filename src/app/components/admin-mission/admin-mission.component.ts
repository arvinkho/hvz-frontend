import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { takeWhile } from 'rxjs';
import { MissionCreateDto } from 'src/app/models/mission-create-dto';
import { MissionDto } from 'src/app/models/mission-dto';
import { GameService } from 'src/app/services/game.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MissionService } from 'src/app/services/mission.service';

@Component({
  selector: 'app-admin-mission',
  templateUrl: './admin-mission.component.html',
  styleUrls: ['./admin-mission.component.css']
})
export class AdminMissionComponent implements OnInit {

  setMissionForm: FormGroup;
  editMissionForm: FormGroup;
  gameIdForm: FormGroup;

  constructor(
    public missionService: MissionService,
    public gameService: GameService,
    private loadingService: LoadingService,
  ) { 

    this.gameIdForm = new FormGroup({
			gameId: new FormControl(""),
		});

    this.setMissionForm = new FormGroup({
			inputMissionId: new FormControl(""),
		});

    this.editMissionForm = new FormGroup({
			inputMissionName: new FormControl(""),
			inputGameId: new FormControl(""),
			humanChecked: new FormControl(""),
			zombieChecked: new FormControl(""),
			inputMissionDescription: new FormControl(""),
			inputNwLat: new FormControl(""),
			inputNwLong: new FormControl(""),
      inputSeLat: new FormControl(""),
      inputSeLong: new FormControl(""),
      inputStartTime: new FormControl(""),
      inputEndTime: new FormControl(""),
		});
  }

  ngOnInit(): void {
    this.gameService.getAllGames();
  }

  public onCreateMission(createNewMissionForm :NgForm): void {
    const { inputMissionName, inputGameId, humanChecked, zombieChecked, inputMissionDescription, 
      inputNwLat, inputNwLong, inputSeLat, inputSeLong, inputStartTime, inputEndTime } = createNewMissionForm.value;
    const mission = new MissionCreateDto(inputMissionName, humanChecked, zombieChecked, 
      inputGameId, inputMissionDescription, inputNwLat, inputNwLong, inputSeLat, inputSeLong, Date.parse(inputStartTime), Date.parse(inputEndTime));
    this.missionService.createMission(inputGameId, mission);
    createNewMissionForm.resetForm();
  }

  public onGameIdSubmit(): void {
    const gameId: number = this.gameIdForm.value.gameId;
    this.missionService.getAllMissions(gameId);
  }

  public setEditMission(): void {
    const gameId: number = this.gameIdForm.value.gameId;
    const missionId: number = this.setMissionForm.value.inputMissionId;
    this.missionService.getSingleMission(gameId, missionId);
    this.loadingService.loading$.pipe(takeWhile( loading => this.loadingService.loading == true))
		.subscribe({
			complete: () => {
				if (missionId === null) {
					this.editMissionForm.patchValue({
						inputMissionName: (""),
						humanChecked: (""),
            zombieChecked: (""),
						inputMissionDescription: (""),
						inputNwLat: (""),
						inputNwLong: (""),
						inputSeLat: (""),
						inputSeLong:(""),
            inputStartTime:(""),
            inputEndTime:(""),
					});
				} else {
          const startTimeISO = new Date(this.missionService.singleMission.startTime).toISOString();
          const endTimeISO = new Date(this.missionService.singleMission.endTime).toISOString();
					this.editMissionForm.patchValue({
						inputMissionName: this.missionService.singleMission.name,
						inputGameId: this.missionService.singleMission.gameId,
						humanChecked: this.missionService.singleMission.isHumanVisible,
            zombieChecked: this.missionService.singleMission.isZombieVisible,
            inputMissionDescription: this.missionService.singleMission.description,
						inputNwLat: this.missionService.singleMission.nwLatitude,
						inputNwLong: this.missionService.singleMission.nwLongitude,
						inputSeLat: this.missionService.singleMission.seLatitude,
						inputSeLong: this.missionService.singleMission.seLongitude,
            inputStartTime: this.getFormattedDatetime(startTimeISO),
            inputEndTime: this.getFormattedDatetime(endTimeISO),
					})
				}
        console.log(this.missionService.singleMission);
				console.log(this.editMissionForm.value);
			  }
		})
  }

  public onSubmitEditMission(): void {
    const gameId: number = this.gameIdForm.value.gameId;
    const missionId: number = this.setMissionForm.value.inputMissionId;
    const editedMission: MissionCreateDto = new MissionCreateDto(
			this.editMissionForm.value.inputMissionName,
			this.editMissionForm.value.humanChecked,
      this.editMissionForm.value.zombieChecked,
      gameId,
      this.editMissionForm.value.inputMissionDescription,
			this.editMissionForm.value.inputNwLat,
			this.editMissionForm.value.inputNwLong,
			this.editMissionForm.value.inputSeLat,
			this.editMissionForm.value.inputSeLong,
      Date.parse(this.editMissionForm.value.inputStartTime),
      Date.parse(this.editMissionForm.value.inputEndTime),
		);
    
		this.missionService.updateMission(gameId, missionId, editedMission);

		console.log("Game ID: " + this.gameIdForm.value.gameId);
		console.log("Updated game" + this.editMissionForm.value);
    this.editMissionForm.reset();
  }

  private getFormattedDatetime = (dateString: string) => {
    const d = new Date(dateString);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    let hour = '' + d.getHours();
    let min = '' + d.getMinutes();
    let sec = '' + d.getSeconds();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour.length < 2) hour = '0' + hour;
    if (min.length < 2) min = '0' + min;
    if (sec.length < 2) sec = '0' + sec;
    return [year, month, day].join('-')+'T'+[hour,min,sec].join(':');
} 

}
