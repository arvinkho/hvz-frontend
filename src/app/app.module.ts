import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AuthModule, AuthHttpInterceptor } from '@auth0/auth0-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LandingPage } from './pages/landing/landing.page';
import { GameDetailPage } from './pages/game-detail/game-detail.page';
import { AdminViewPage } from './pages/admin-view/admin-view.page';

import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { environment as env } from 'src/environments/environment';
import { NavbarComponent } from './navbar/navbar.component';
import { ChatComponent } from './components/chat/chat.component';
import { GameListComponent } from './components/game-list/game-list.component';
import { GameListItemComponent } from './components/game-list-item/game-list-item.component';
import { AuthButtonComponent } from './components/auth-button/auth-button.component';
import { PlayerStatesComponent } from './components/player-states/player-states.component';
import { PlayerStatesListComponent } from './components/player-states-list/player-states-list.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { PlayerStatesListItemComponent } from './components/player-states-list-item/player-states-list-item.component';
import { GameDetailsComponent } from './components/game-details/game-details.component';
import { AdminGameComponent } from './components/admin-game/admin-game.component';
import { AdminMissionComponent } from './components/admin-mission/admin-mission.component';
import { BiteComponent } from './components/bite/bite.component';
import { HumanZombiePipe } from './pipes/human-zombie.pipe';



import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MapComponent } from './map/map.component';
import { MarkerService } from './services/mapApi/marker.service';
import { PopupService } from './services/mapApi/popup.service';
import { MapService } from './services/mapApi/map.service';
import { LoadingComponent } from './components/loading/loading.component';
import { ExternalApiComponent } from './components/external-api/external-api.component';
import { SquadRegistrationComponent } from './components/squad-registration/squad-registration.component';
import { SquadListItemComponent } from './components/squad-list-item/squad-list-item.component';
import { SquadDetailsComponent } from './components/squad-details/squad-details.component';
import { SquadMemberListComponent } from './components/squad-member-list/squad-member-list.component';
import { ToastrModule } from 'ngx-toastr';
import { LeafletMapComponent } from './leaflet-map/leaflet-map.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';


@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    AuthButtonComponent,
    UserProfileComponent,
    ExternalApiComponent,
    LandingPage,
    GameDetailPage,
    AdminViewPage,
    ChatComponent,
    GameListComponent,
    NavbarComponent,
    GameListItemComponent,
    PlayerStatesComponent,
    PlayerStatesListComponent,
    SearchbarComponent,
    PlayerStatesListItemComponent,
    GameDetailsComponent,
    AdminGameComponent,
    AdminMissionComponent,
    MapComponent,
    GameDetailsComponent,
    BiteComponent,
    LoadingComponent,
    HumanZombiePipe,
    SquadRegistrationComponent,
    SquadListItemComponent,
    SquadDetailsComponent,
    SquadMemberListComponent,
    LeafletMapComponent,
  ],
  imports: [
    LeafletModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    AuthModule.forRoot({
      ... env.auth,
      httpInterceptor: {
        allowedList: [
          {
            uri: `${env.dev.serverUrl}${env.dev.endpoints.gameApi}`,
            allowAnonymous: true
          },
          `${env.dev.serverUrl}/*`
        ] 
      }
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    },
    MarkerService,
    PopupService,
    MapService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
