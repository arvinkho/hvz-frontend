export class GameCreateDto {
    name: string;
    gameState: string;
    description: string;
    nwLatitude: number;
    nwLongitude: number;
    seLatitude: number;
    seLongitude: number;

    constructor(name: string, description: string, nwLatitude: number, nwLongitude: number, seLatitude: number, seLongitude: number, gameState: string) {
        this.name = name;
        this.description = description;
        this.nwLatitude = nwLatitude;
        this.nwLongitude = nwLongitude;
        this.seLatitude = seLatitude;
        this.seLongitude = seLongitude;
        this.gameState = gameState;
    }
}