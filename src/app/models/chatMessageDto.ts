export class ChatMessageDto {
    userName: string;
    avatarUrl?: string;
    userId?: number;
    message: string;
    isHumanGlobal?: boolean;
    isZombieGlobal?: boolean;
    squadId?: number;
    gameId?: number;


    

    constructor(userName: string, message: string, avatarUrl: string, userId?: number, isHumanGlobal?: boolean,
         isZombieGlobal?: boolean, squadId?: number, gameId?: number) {
        this.userName = userName;
        this.avatarUrl = avatarUrl;
        this.userId = userId;
        this.message = message;
        this.isHumanGlobal = isHumanGlobal;
        this.isZombieGlobal = isZombieGlobal;
        this.squadId = squadId;
        this.gameId = gameId;
    }

    public setUserId(userId: number) {
        this.userId = userId;
    }

    public setIsHumanGlobal(isHumanGlobal: boolean) {
        this.isHumanGlobal = isHumanGlobal;
    }

    public setIsZombieGlobal(isZombieGlobal: boolean) {
        this.isZombieGlobal = isZombieGlobal;
    }

    public setSquadId(squadId: number) {
        this.squadId = squadId;
    }

    public setGameId(gameId: number) {
        this.gameId = gameId;
    }
}