export class SquadCheckinCreateDto {
    latitude: number;
    longitude: number;
    gameId: number;
    squadId: number;
    squadMemberId: number;

    constructor(latitude: number, longitude: number, gameId: number, squadId: number, squadMemberId: number) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.gameId = gameId;
        this.squadId = squadId;
        this.squadMemberId = squadMemberId;
    }
}