import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { GameDto } from 'src/app/models/game-dto';
import { GameService } from 'src/app/services/game.service';
import { UserService } from 'src/app/services/user.service';



@Component({
  selector: 'app-game-list-item',
  templateUrl: './game-list-item.component.html',
  styleUrls: ['./game-list-item.component.css']
})
export class GameListItemComponent implements OnInit {

  @Input() game?: GameDto;

  constructor(
    private router: Router,
    private gameService: GameService,
    public userService: UserService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {

  }

  onJoinClick(): void {
    this.gameService.setSingleGame(this.game!);
    this.router.navigate(["/game-detail", this.game?.gameId]);
  }

}
