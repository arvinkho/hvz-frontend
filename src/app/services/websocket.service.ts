import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ChatMessageDto } from '../models/chatMessageDto';

import { AuthService } from "@auth0/auth0-angular";
import { take } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  webSocket?: WebSocket;
  chatMessages: ChatMessageDto[] = [];

  constructor(public auth: AuthService) { }

  public openWebSocket() {
      this.auth.getAccessTokenSilently().pipe(take(1)).subscribe(token => {        
        this.webSocket = new WebSocket(environment.SOCKET_ENDPOINT, token);
    
        this.webSocket.onopen = (event) => {
          console.log("Open: ", event);
        };
    
        this.webSocket.onmessage = (event) => {
          const chatMessageDto = JSON.parse(event.data);
          this.chatMessages.push(chatMessageDto);
        };
    
        this.webSocket.onclose = (event) => {
          console.log("Close: ", event);
        };
      }
    )
  }

  public sendMessage(chatMessageDto: ChatMessageDto) {
    this.webSocket?.send(JSON.stringify(chatMessageDto));
  }

  public closeWebSocket() {
    this.webSocket?.close();
  }

}
