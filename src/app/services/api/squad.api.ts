import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SquadCheckinCreateDto } from "src/app/models/squad-checkin-create-dto";
import { SquadCheckinDto } from "src/app/models/squad-checkin-dto";
import { SquadCreateDto } from "src/app/models/squad-create-dto";
import { SquadDto } from "src/app/models/squad-dto";
import { SquadMemberCreateDto } from "src/app/models/squad-member-create-dto";
import { SquadMemberDto } from "src/app/models/squad-member-dto";
import { environment as env } from "src/environments/environment";

const API_URL = env.dev.serverUrl + env.dev.endpoints.gameApi;

@Injectable({
    providedIn: "root"
})
// DO NOT INJECT, ONLY USED FOR SQUAD SERVICE
export class SquadAPI {

    constructor(private http: HttpClient) {}

    public getAllSquads(gameId: number): Observable<SquadDto[]> {
        return this.http.get<SquadDto[]>(API_URL + "/" + gameId + "/squad");
    }

    public getSingleSquad(squadId: number, gameId: number): Observable<SquadDto> {
        return this.http.get<SquadDto>(API_URL + "/" + gameId + "/squad/" + squadId);
    }

    public getSingleSquadBySquadMember(squadMember: number, gameId: number): Observable<SquadDto> {
        return this.http.get<SquadDto>(API_URL + "/" + gameId + "/squad/squadmember/" + squadMember);
    }

    public createSquad(newSquad: SquadCreateDto, gameId: number): Observable<SquadDto> {
        return this.http.post<SquadDto>(API_URL + "/" + gameId + "/squad/", newSquad);
    }

    public createSquadMember(newSquadMember: SquadMemberCreateDto, gameId: number, squadId: number): Observable<SquadMemberDto> {
        return this.http.post<SquadMemberDto>(API_URL + "/" + gameId + "/squad/" + squadId, newSquadMember);
    }

    public updateSquad(squadId: number, updatedSquad: SquadCreateDto, gameId: number): Observable<SquadDto> {
        return this.http.put<SquadDto>(API_URL + "/" + gameId + "/squad/" + squadId, updatedSquad);
    }

    public deleteSquad(squadId: number, gameId: number): Observable<unknown> {
        return this.http.delete(API_URL + "/" + gameId + "/squad/" + squadId);
    }

    public deleteSquadMember(squadId: number, gameId: number, squadMemberId: number): Observable<unknown> {
        return this.http.delete(API_URL + "/" + gameId + "/squad/" + squadId + "/squadmember/" + squadMemberId);
    }

    public getSquadCheckins(squadId: number, gameId: number): Observable<SquadCheckinDto[]> {
        return this.http.get<SquadCheckinDto[]>(API_URL + "/" + gameId + "/squad/" + squadId + "/check-in");
    }

    public createSquadCheckin(squadId: number, gameId: number, squadCheckin: SquadCheckinCreateDto): Observable<SquadCheckinDto> {
        return this.http.post<SquadCheckinDto>(API_URL + "/" + gameId + "/squad/" + squadId + "/check-in", squadCheckin);
    }
}