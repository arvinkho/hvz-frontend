import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { KillCreateDto } from "src/app/models/kill-create-dto";
import { KillDto } from "src/app/models/kill-dto";
import { environment as env } from "src/environments/environment";

const API_URL = env.dev.serverUrl + env.dev.endpoints.gameApi;

@Injectable({
    providedIn: "root"
})

// DO NOT INJECT, ONLY USED FOR KILL SERVICE
export class KillAPI {
    constructor(private http: HttpClient) {}

    public getAllKillsInGame(gameId: number): Observable<KillDto[]> {
        return this.http.get<KillDto[]>(API_URL + "/" + gameId + "/kill");
    }

    public getSingleKill(gameId: number, killId: number): Observable<KillDto> {
        return this.http.get<KillDto>(API_URL + "/" + gameId + "/kill/" + killId);
    }

    public createKill(gameId: number, newKill: KillCreateDto): Observable<KillDto> {
        return this.http.post<KillDto>(API_URL + "/" + gameId + "/kill", newKill);
    }

    public updateKill(gameId: number, killId: number, newKill: KillCreateDto): Observable<KillDto> {
        return this.http.put<KillDto>(API_URL + "/" + gameId + "/kill/" + killId, newKill);
    }

    public deleteKill(gameId: number, killId: number): Observable<unknown> {
        return this.http.delete(API_URL + "/" + gameId + "/kill/" + killId);
    }

}