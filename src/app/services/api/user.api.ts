import { HttpClient, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { UserCreateDto } from "src/app/models/user-create-dto";
import { UserDto } from "src/app/models/user-dto";
import { environment as env } from "src/environments/environment";

const API_URL = env.dev.serverUrl + env.dev.endpoints.userApi;

@Injectable({
    providedIn: "root"
})
// DO NOT INJECT, ONLY USED FOR USER SERVICE
export class UserAPI {


    constructor(private http: HttpClient) {}

    public getSingleUser(userId: string) {
        return this.http.get<UserDto>(API_URL + "/" + userId);
    }

    public createUser(newUser: UserCreateDto) {
        return this.http.post<UserCreateDto>(API_URL, newUser);
    }

    public setGame(gameId: number) {
        return this.http.put<UserDto>(API_URL, gameId);
    }
}