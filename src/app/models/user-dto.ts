export class UserDto {

    userId: string;
    name: string;
    email: string;
    playerId?: number;

    constructor(userId: string, name: string, email: string, playerId?: number) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.playerId = playerId;
    }
}