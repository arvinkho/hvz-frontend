import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  private _lastErrorSubject: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private _lastError: string = "";
  private _errorLog: string[] = [];
  private _errorLogSubject: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([])

  get lastError(): string {
    return this._lastError;
  }

  get lastError$(): Observable<string> {
    return this._lastErrorSubject.asObservable();
  }

  get errorLog(): string[] {
    return this._errorLog;
  }

  get errorLog$(): Observable<string[]> {
    return this._errorLogSubject.asObservable();
  }

  constructor() { }

  public setError(errorMessage: string): void {
    errorMessage = (new Date).toISOString() + ": " + errorMessage;
    this._lastError = errorMessage;
    this._lastErrorSubject.next(errorMessage);
    this._errorLog.push(errorMessage);
    this._errorLogSubject.next(this._errorLog);
  }

}
