export class SquadMemberCreateDto {
    private gameId: number;
    private squadId: number;
    private playerId: number;

    constructor(gameId: number, squadId: number, playerId: number) {
        this.gameId = gameId;
        this.squadId = squadId;
        this.playerId = playerId;
    }
}