export class SquadCreateDto {

    name: string;
    gameId: number;

    constructor(name: string, gameId: number) {
        this.name = name;
        this.gameId = gameId;
    }

}