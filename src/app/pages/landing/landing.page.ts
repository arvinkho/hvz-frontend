import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { GameDto } from 'src/app/models/game-dto';
import { UserModel } from 'src/app/models/user-model';
import { GameService } from 'src/app/services/game.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.css']
})
export class LandingPage implements OnInit {
  get games(): GameDto[] {
    return this.gameService.allGames;
  }

  constructor(
    private readonly gameService: GameService,
    private userService: UserService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    this.gameService.getAllGames();
  }

}
