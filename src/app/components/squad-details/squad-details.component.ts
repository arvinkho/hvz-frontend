import { Component, HostListener, Input, OnInit } from '@angular/core';
import { GameDto } from 'src/app/models/game-dto';
import { SquadDto } from 'src/app/models/squad-dto';
import { PlayerService } from 'src/app/services/player.service';
import { SquadService } from 'src/app/services/squad.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-squad-details',
  templateUrl: './squad-details.component.html',
  styleUrls: ['./squad-details.component.css']
})
export class SquadDetailsComponent implements OnInit {

  public currentWindowWidth: number;

  @Input() _game: GameDto;
  @Input() _squad: SquadDto;


  constructor(
    public userService: UserService,
    private squadService: SquadService,
    public playerService: PlayerService
  ) { }

  ngOnInit(): void {
    this.playerService.getAllPlayersInSquad(this._game.gameId, this._squad.squadId);
    this.currentWindowWidth = window.innerWidth;
  }

  public onCheckin(): void {
    this.userService.squadCheckin();
  }

  public onLeaveSquad(): void {
    this.userService.leaveSquad();
    this.squadService.getAllSquads(this._game!.gameId);
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth
  }

}
