import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { AuthGuard } from '@auth0/auth0-angular';
import { LandingPage } from './pages/landing/landing.page';
import { ExternalApiComponent } from './components/external-api/external-api.component';
import { AdminViewPage } from './pages/admin-view/admin-view.page';
import { MapComponent } from './map/map.component';
import { GameDetailPage } from './pages/game-detail/game-detail.page';
import { ChatComponent } from './components/chat/chat.component';
import { AdminGuard } from './guards/admin.guard';
import { LeafletMapComponent } from './leaflet-map/leaflet-map.component';


const routes: Routes = [
  {
    path: "",
    component: LandingPage
  },
  {
    path: "profile",
    component: UserProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "external-api",
    component: ExternalApiComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "admin",
    component: AdminViewPage,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path:"map",
    component: MapComponent
  },
  {
    path: "game-detail/:game_id",
    component: GameDetailPage
  },
    {
      path: "game-detail/:game_id",
      component: GameDetailPage
    },
  {
    path: "chat",
    component: ChatComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
