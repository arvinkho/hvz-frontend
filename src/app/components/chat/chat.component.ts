import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { take } from 'rxjs';
import { ChatMessageDto } from 'src/app/models/chatMessageDto';
import { UserModel } from 'src/app/models/user-model';
import { GameService } from 'src/app/services/game.service';
import { PlayerService } from 'src/app/services/player.service';
import { SquadService } from 'src/app/services/squad.service';
import { UserService } from 'src/app/services/user.service';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  public chatScope?: string = 'Global';
  private _user!: UserModel;

  constructor(public webSocketService: WebsocketService, public userService: UserService,
     public playerService: PlayerService, public squadService: SquadService, public gameService: GameService) {
  }

  ngOnInit(): void {
    this.webSocketService.openWebSocket();
  }

  ngOnDestroy(): void {
    this.webSocketService.closeWebSocket();
  }

  sendMessage(sendForm: NgForm) {
    this.userService.user$.pipe(take(1))
    .subscribe({
      next: user => {
        this._user = user;
      } 
    })
    
    
    const chatMessageDto = new ChatMessageDto(this._user.name!, sendForm.value.message, this._user.avatarUrl!);
    chatMessageDto.setGameId(this.gameService.singleGame.gameId);
    switch (this.chatScope) {
     
      case 'Zombie':
        chatMessageDto.setIsHumanGlobal(false);
        chatMessageDto.setIsZombieGlobal(true);
        break;

      case 'Human':
        chatMessageDto.setIsHumanGlobal(true);
        chatMessageDto.setIsZombieGlobal(false);
        break;

      case 'Squad':
        chatMessageDto.setIsHumanGlobal(false);
        chatMessageDto.setIsZombieGlobal(false);
        if (this.squadService.singleSquad) {
          chatMessageDto.setSquadId(this.squadService.singleSquad?.squadId);
        }
        break;

      default:
        chatMessageDto.setIsHumanGlobal(true);
        chatMessageDto.setIsZombieGlobal(true);
        break;
    }
    this.webSocketService.sendMessage(chatMessageDto);
    sendForm.controls['message'].reset();
  }

  selectChatScope(chatScope: string){
    this.chatScope = chatScope;
    console.log(this.chatScope);
    
  }

}
