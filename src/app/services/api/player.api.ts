import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PlayerCreateDto } from "src/app/models/player-create-dto";
import { PlayerDto } from "src/app/models/player-dto";
import { environment as env } from "src/environments/environment";

const API_URL = env.dev.serverUrl + env.dev.endpoints.gameApi;

@Injectable({
    providedIn: "root"
})
// DO NOT INJECT, ONLY USED FOR PLAYER SERVICE
export class PlayerAPI {
    constructor(private http: HttpClient) {}

    public getAllPlayers(): Observable<PlayerDto[]> {
        return this.http.get<PlayerDto[]>(API_URL + "/player");
    }

    public getAllPlayersInGame(gameId: number): Observable<PlayerDto[]> {
        return this.http.get<PlayerDto[]>(API_URL + "/" + gameId + "/player");
    }

    public getAllPlayersInSquad(gameId: number, squadId: number): Observable<PlayerDto[]> {
        return this.http.get<PlayerDto[]>(API_URL + "/" + gameId + "/squad/" + squadId + "/player");
    }

    public getSinglePlayerInGame(gameId: number, playerId: number): Observable<PlayerDto> {
        return this.http.get<PlayerDto>(API_URL + "/" + gameId + "/player/" + playerId);
    }

    public getSinglePlayer(playerId: number): Observable<PlayerDto> {
        return this.http.get<PlayerDto>(API_URL + "/player/" + playerId);
    }

    public createPlayer(gameId: number, newPlayer: PlayerCreateDto): Observable<PlayerDto> {
        return this.http.post<PlayerDto>(API_URL + "/" + gameId + "/player", newPlayer);
    }

    public updatePlayer(gameId: number, playerId: number, human: boolean): Observable<PlayerCreateDto> {
        return this.http.put<PlayerCreateDto>(API_URL + "/" + gameId + "/player/" + playerId, human);
    }

    public deletePlayer(gameId: number, playerId: number): Observable<unknown> {
        return this.http.delete(API_URL + "/" + gameId + "/player/" + playerId);
    }

}