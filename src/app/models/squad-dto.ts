export class SquadDto {

    private _squadId: number;
    private _name: string;
    private _isHuman: boolean;
    private _gameId: number;
    private _squadMemberIds: number[];
    private _squadCheckinIds: number[];
    private _humanCount: number;
    private _memberCount: number;

    get squadId(): number {
        return this._squadId;
    }

    get name(): string {
        return this._name;
    }

    get isHuman(): boolean {
        return this._isHuman;
    } 

    get gameId(): number {
        return this._gameId;
    }

    get squadMemberIds(): number[] {
        return this._squadMemberIds;
    }

    get squadCheckinIds(): number[] {
        return this._squadCheckinIds;
    }

    get humanCount(): number {
        return this._humanCount;
    }

    get memberCount(): number {
        return this._memberCount;
    }

    constructor(squadId: number, name: string, isHuman: boolean, gameId: number, squadMemberIds: number[], 
        squadCheckinIds: number[], humanCount: number, memberCount: number) {
        this._squadId = squadId;
        this._name = name;
        this._isHuman = isHuman;
        this._gameId = gameId;
        this._squadMemberIds = squadMemberIds;
        this._squadCheckinIds = squadCheckinIds;
        this._humanCount = humanCount;
        this._memberCount = memberCount;
    }
}