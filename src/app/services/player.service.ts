import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { PlayerCreateDto } from '../models/player-create-dto';
import { PlayerDto } from '../models/player-dto';
import { PlayerAPI } from './api/player.api';
import { ErrorService } from './error.service';
import { LoadingService } from './loading.service';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private _allPlayers: PlayerDto[] = [];
  private _allPlayersSubject: BehaviorSubject<PlayerDto[]> = new BehaviorSubject<PlayerDto[]>([]);
  private _allPlayersInGame: PlayerDto[] = [];
  private _allPlayersInGameSubject: BehaviorSubject<PlayerDto[]> = new BehaviorSubject<PlayerDto[]>([]);
  private _allPlayersInSquad: PlayerDto[] = [];
  private _allPlayersInSquadSubject: BehaviorSubject<PlayerDto[]> = new BehaviorSubject<PlayerDto[]>([]);
  private _singlePlayer!: PlayerDto;
  private _singlePlayerSubject: BehaviorSubject<PlayerDto | null | undefined> = new BehaviorSubject<PlayerDto | null | undefined>(null);
  private _error: string = "";
  private _loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  get allPlayers() {
    return this._allPlayers;
  }

  get allPlayers$() {
    return this._allPlayersSubject.asObservable();
  }

  get allPlayersInGame() {
    return this._allPlayersInGame;
  }

  get allPlayersInGame$() {
    return this._allPlayersInGameSubject.asObservable();
  }

  get allPlayersInSquad() {
    return this._allPlayersInSquad;
  }

  get allPlayersInSquad$() {
    return this._allPlayersInSquadSubject.asObservable();
  }

  get singlePlayer() {
    return this._singlePlayer;
  }

  get singlePlayer$() {
    return this._singlePlayerSubject.asObservable();
  }

  get error() {
    return this._error;
  }

  get loading$() {
    return this._loadingSubject.asObservable();
  }

  constructor(private playerAPI: PlayerAPI, private loadingService: LoadingService, private errorService: ErrorService, private toastr: ToastrService) { }

  public reset(): void {
    this._singlePlayerSubject.next(null);
  }

  

  public getSinglePlayer(playerId: number): void {
    this.loadingService.startLoading();
    this.playerAPI.getSinglePlayer(playerId).subscribe({
      next: (player: PlayerDto) => {
        this._singlePlayer = player;
        this._singlePlayerSubject.next(player);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public getAllPlayers(): void {
    this.loadingService.startLoading();
    this.playerAPI.getAllPlayers().subscribe({
      next: (players: PlayerDto[]) => {
        this._allPlayers = players;
        this._allPlayersSubject.next(players);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public getAllPlayersInGame(gameId: number): void {
    this.loadingService.startLoading();
    this.playerAPI.getAllPlayersInGame(gameId).subscribe({
      next: (players: PlayerDto[]) => {
        this._allPlayersInGame = players;
        this._allPlayersInGameSubject.next(players);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public getAllPlayersInSquad(gameId: number, squadId: number): void {
    this.loadingService.startLoading();
    this.playerAPI.getAllPlayersInSquad(gameId, squadId).subscribe({
      next: (players: PlayerDto[]) => {
        this._allPlayersInSquad = players;
        this._allPlayersInSquadSubject.next(players);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public createPlayer(gameId: number, newPlayer: PlayerCreateDto): void {
    this.loadingService.startLoading();
    this.playerAPI.createPlayer(gameId, newPlayer).subscribe({
      next: (player: PlayerDto) => {
        this._singlePlayer = player;
        this._singlePlayerSubject.next(player);
        this.toastr.success("Successfully created a player with name " + player.name, "Register for game successful!");
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 400) {
          this.toastr.error("Game not found, or player is already registered in a different game", "Register for game failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public updatePlayer(gameId: number, playerId: number, human: boolean): void {
    this.loadingService.startLoading();
    this.playerAPI.updatePlayer(gameId, playerId, human).subscribe({
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 400) {
          this.toastr.error("Game not found, or player is already registered in a different game", "Register for game failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
        this.toastr.success("Successfully created a player", "Register for game successful!");
      }
    })
  }

  public deletePlayer(gameId: number, playerId: number) {
    this.loadingService.startLoading();
    this.playerAPI.deletePlayer(gameId, playerId).subscribe({
      error: (error:HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 404) {
          this.toastr.error("Game not found, or player is already registered in a different game", "Deletion of player failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
        this.toastr.success("Successfully unregistered from the game", "Unregister for game successful!");
      }
    })
  }
}
