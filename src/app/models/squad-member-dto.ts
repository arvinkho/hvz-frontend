export class SquadMemberDto {
    private _squadMemberId: number;
    private _gameId: number;
    private _squadId: number;
    private _playerId: number;

    get squadMemberId(): number {
        return this._squadMemberId;
    }

    get gameId(): number {
        return this._gameId;
    }

    get squadId(): number {
        return this._squadId;
    }

    get playerId(): number {
        return this._playerId;
    }

    constructor(squadMemberId: number, gameId: number, squadId: number, playerId: number) {
        this._squadMemberId = squadMemberId;
        this._gameId = gameId;
        this._squadId = squadId;
        this._playerId = playerId;
    }
}