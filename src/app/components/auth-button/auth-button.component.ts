import { Component, HostListener, Inject, OnInit } from '@angular/core';

import { AuthService } from '@auth0/auth0-angular';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-auth-button',
  templateUrl: "auth-button.component.html",
})
export class AuthButtonComponent implements OnInit{

  public currentWindowWidth: number;

  constructor(@Inject(DOCUMENT) public document: Document, public auth: AuthService) { }

  ngOnInit(): void {
    this.currentWindowWidth = window.innerWidth;
  }
  
  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth
  }
  
}
