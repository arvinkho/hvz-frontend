import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { SquadCheckinCreateDto } from '../models/squad-checkin-create-dto';
import { SquadCheckinDto } from '../models/squad-checkin-dto';
import { SquadCreateDto } from '../models/squad-create-dto';
import { SquadDto } from '../models/squad-dto';
import { SquadMemberCreateDto } from '../models/squad-member-create-dto';
import { SquadAPI } from './api/squad.api';
import { ErrorService } from './error.service';
import { LoadingService } from './loading.service';

@Injectable({
  providedIn: 'root'
})
export class SquadService {

  private _allSquads: SquadDto[] = [];
  private _allSquadsSubject: BehaviorSubject<SquadDto[]> = new BehaviorSubject<SquadDto[]>([]);
  private _singleSquad?: SquadDto;
  private _singleSquadSubject: BehaviorSubject<SquadDto | null | undefined> = new BehaviorSubject<SquadDto | null | undefined>(null);
  private _squadCheckins: BehaviorSubject<SquadCheckinDto[]> = new BehaviorSubject<SquadCheckinDto[]>([]);
  private _error: string = "";
  private _loading: boolean = false;

  get allSquads() {
    return this._allSquads;
  }

  get allSquads$() {
    return this._allSquadsSubject.asObservable();
  }

  get singleSquad() {
    return this._singleSquad;
  }

  get singleSquad$() {
    return this._singleSquadSubject.asObservable();
  }

  get squadCheckins$() {
    return this._squadCheckins.asObservable();
  }

  get error() {
    return this._error;
  }

  get loading() {
    return this._loading;
  }

  constructor(private squadAPI: SquadAPI, private loadingService: LoadingService, private errorService: ErrorService, private toastr: ToastrService) { }

  public getSingleSquad(squadId: number, gameId: number): void {
    this.loadingService.startLoading();
    this.squadAPI.getSingleSquad(squadId, gameId).subscribe({
      next: (squad: SquadDto) => {
        this._singleSquad = squad;
        this._singleSquadSubject.next(squad);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public getSingleSquadBySquadMember(squadMemberId: number, gameId: number): void {
    this.loadingService.startLoading();
    this.squadAPI.getSingleSquadBySquadMember(squadMemberId, gameId).subscribe({
      next: (squad: SquadDto) => {
        this._singleSquad = squad;
        this._singleSquadSubject.next(squad);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }


  public getAllSquads(gameId: number): void {
    this.loadingService.startLoading();
    this.squadAPI.getAllSquads(gameId).subscribe({
      next: (squads: SquadDto[]) => {
        this._allSquads = squads;
        this._allSquadsSubject.next(squads);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public createSquad(newSquad: SquadCreateDto, gameId: number): void {
    this.loadingService.startLoading();
    this.squadAPI.createSquad(newSquad, gameId).subscribe({
      next: squad => {
        this._singleSquadSubject.next(squad);
        this.toastr.success("Successfully created a squad with the name " + squad.name, "Squad created successfully!");
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 404) {
          this.toastr.error("The player is already in a squad.", "Squad creation failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public createSquadMember(newSquadMember: SquadMemberCreateDto, gameId: number, squadId: number): void {
    this.loadingService.startLoading();
    this.squadAPI.createSquadMember(newSquadMember, gameId, squadId).subscribe({
      next: squad => {
        if (squad) {
          this.getSingleSquad(squad.squadId, squad.gameId);
          this.toastr.success("Successfully joined the squad", "Squad join successful!");
        }
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 404) {
          this.toastr.error("Squad not found.", "Squad join failed")
        }
        if (error.status === 400) {
          this.toastr.error("Player is already in a squad.", "Squad join failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public updateSquad(squadId: number, updatedSquad: SquadCreateDto, gameId: number): void {
    this.loadingService.startLoading();
    this.squadAPI.updateSquad(squadId, updatedSquad, gameId).subscribe({
      next: (squad: SquadDto) => {
        this.toastr.success("Successfully updated the squad " + squad.name, "Squad update successful!");
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 400) {
          this.toastr.error("Squad not found.", "Squad join failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public deleteSquad(squadId: number, gameId: number): void {
    this.loadingService.startLoading();
    this.squadAPI.deleteSquad(squadId, gameId).subscribe({
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 404) {
          this.toastr.error("Squad not found.", "Squad delete failed")
        }
      },
      complete: () => {
        this.loadingService.stopLoading();
        this.toastr.success("Successfully deleted the squad", "Squad delete successful!");
      }
    })
  }

  public deleteSquadMember(squadId: number, gameId: number, squadMemberId: number): void {
    this.loadingService.startLoading();
    this.squadAPI.deleteSquadMember(squadId, gameId, squadMemberId).subscribe({
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
        if (error.status === 404) {
          this.toastr.error("Squad member not found.", "Squad member delete failed")
        }
      },
      complete: () => {
        //this.getSingleSquad(squadId, gameId);
        this.loadingService.stopLoading();
        this.toastr.success("Successfully left the squad", "Squad leave successful!");
      }
    })
  }

  public getSquadCheckins(squadId: number, gameId: number): void {
    this.loadingService.startLoading();
    this.squadAPI.getSquadCheckins(squadId, gameId).subscribe({
      next: (squadCheckins: SquadCheckinDto[]) => {
        this._squadCheckins.next(squadCheckins);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public createSquadCheckin(squadId: number, gameId: number, squadCheckin: SquadCheckinCreateDto): void {
    this.loadingService.startLoading();
    this.squadAPI.createSquadCheckin(squadId, gameId, squadCheckin).subscribe({
      error: (error: HttpErrorResponse) => {
        this.errorService.setError(error.message);
        this.loadingService.stopLoading();
      },
      complete: () => {
        this.loadingService.stopLoading();
      }
    })
  }

  public reset(): void {
    this._singleSquad = undefined;
    this._singleSquadSubject.next(null);
  }


}
