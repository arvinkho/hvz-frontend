// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  //SOCKET_ENDPOINT: "ws://localhost:8080/chat",
  SOCKET_ENDPOINT: "wss://hvz-java-backend.herokuapp.com/chat",
  auth: {
    //domain: "dev-br-siqfw.eu.auth0.com",
    //clientId: "ZMxrdutlwd8DZCG0puWayXuzg38qRSw6",
    domain: "dev-hvz-auth.eu.auth0.com",
    clientId: "pu8Z2wakKj3UXFgoc1M5BUQX0wqtisT3",
    redirectUri: window.location.origin,
    audience: "https://HvZ-api/",
  },
  dev: {
    //serverUrl: "http://localhost:8080",
    serverUrl: "https://hvz-java-backend.herokuapp.com",
    endpoints: {
      gameApi: "/api/v1/game",
      userApi: "/api/v1/user"
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
