## Background

This project was the final assignment in the Java Fullstack course at Noroff for Experis Academy candidates for the spring 2022 class.

The scope of this project was to create a game administration application to keep track of a game of humans vs zombies. Humans vs zombies is a game of tag where the zombies' goal is to tag and "infect" humans. Upon getting tagged, the human will then become a zombie. Read more about the concept on the humans vs zombies web page: https://humansvszombies.org/.


# Usage

This application was built using Angular and Typescript, with Bootstrap as a styling framework. It represents our front-end of our project. For information about how to use it, please read our user manual (User-manual-hvz).


# Group

The group consisted of 5 people:

* Arvin Khodabandeh
* Erik Loftesnes
* Håkon Holm Erstad
* Simen-André Fosse
* Trym Ellingsen


# HvzFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
