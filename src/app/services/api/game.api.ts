import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GameCreateDto } from 'src/app/models/game-create-dto';
import { GameDto } from 'src/app/models/game-dto';
import { environment as env } from 'src/environments/environment';
import { GameService } from '../game.service';

const API_URL = `${env.dev.serverUrl}${env.dev.endpoints.gameApi}`;

// THis should never be injected to a component. - Only the game service

@Injectable({
  providedIn: 'root',
})
export class GameAPI {
  constructor(private http: HttpClient) {}

  public getAllGames(): Observable<GameDto[]> {
    return this.http.get<GameDto[]>(API_URL);
  }

  public getSingleGame(gameId: number): Observable<GameDto> {
    return this.http.get<GameDto>(API_URL + `/${gameId}`);
  }

  public createGame(newGame: GameCreateDto): Observable<GameDto> {
    return this.http.post<GameDto>(API_URL, newGame);
  }

  public updateGame(gameId: number, updatedGame: GameCreateDto): Observable<GameDto> {
      return this.http.put<GameDto>(API_URL + "/" + gameId, updatedGame);
  }

  public deleteGame(gameId: number): Observable<unknown> {
      return this.http.delete(API_URL + "/" + gameId);
  }
}
