import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private _loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _delayedLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _loading: boolean = false;

  get loading$(): Observable<boolean> {
    return this._loadingSubject.asObservable();
  }

  get delayedLoading$(): Observable<boolean> {
    return this._delayedLoading.asObservable();
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor() { }

  public startLoading(): void {
    this._loading = true;
    this._loadingSubject.next(true);
    setTimeout(() => {
      if (this._loading) {
        this._delayedLoading.next(true);
      };
    }, 500)
  }

  public stopLoading(): void {
    this._loading = false;
    this._loadingSubject.next(false);
    this._delayedLoading.next(false);
  }

}
