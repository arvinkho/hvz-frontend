import { Component, Input, OnInit } from '@angular/core';
import { GameDto } from 'src/app/models/game-dto';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  @Input() gameList: GameDto[] = [];

  constructor(
    public userService: UserService,
  ) { }

  ngOnInit(): void {
  }

}
