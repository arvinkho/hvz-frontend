import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerStatesListComponent } from './player-states-list.component';

describe('PlayerStatesListComponent', () => {
  let component: PlayerStatesListComponent;
  let fixture: ComponentFixture<PlayerStatesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerStatesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerStatesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
