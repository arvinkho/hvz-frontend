import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor() { }
  makeCapitalPopUp(data:any) : string {
    return `` + 
    `<div>capital: ${data.capital}<div>` +
    `<div>state: ${data.state}<div>` +
    `<div>population: ${data.population}<div>`
  }

  makeDeathMarkerPopUp(data: any) : string {
    return `` +
    `<div>userName: ${data.userName}<div>`+
    `<div>Time of death: ${data.timeOfDeath}<div>`+
    `<div>story: ${data.story}<div>`+
    `<div>avatar: ${data.avatar}<div>`
  }
}
