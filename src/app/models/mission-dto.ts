export class MissionDto {

    missionId: number;
    name: string;
    isHumanVisible: boolean;
    isZombieVisible: boolean;
    description: string;
    nwLatitude: number;
    nwLongitude: number;
    seLatitude: number;
    seLongitude: number;
    startTime: number;
    endTime: number;
    gameId: number;


	constructor(missionId: number, name: string, isHumanVisible: boolean, isZombieVisible: boolean, description: string,
        nwLat: number, nwLong: number, seLat: number, seLong: number, startTime: number, endTime: number, gameId: number) {
        this.missionId = missionId;
        this.name = name;
        this.isHumanVisible = isHumanVisible;
        this.isZombieVisible = isZombieVisible;
        this.description = description;
        this.nwLatitude = nwLat;
        this.nwLongitude = nwLong;
        this.seLatitude = seLat;
        this.seLongitude = seLong;
        this.startTime = startTime;
        this.endTime = endTime;
        this.gameId = gameId;
	}
    
}