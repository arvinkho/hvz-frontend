import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LatLng } from 'leaflet';
import { KillCreateDto } from 'src/app/models/kill-create-dto';
import { KillService } from 'src/app/services/kill.service';
import { MapService } from 'src/app/services/mapApi/map.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-bite',
  templateUrl: './bite.component.html',
  styleUrls: ['./bite.component.css']
})
export class BiteComponent implements OnInit {

  stringPos: string;
  private _curPos?: LatLng;

  constructor(
    private killService: KillService,
    public userService: UserService,
    public mapService: MapService
  ) { }

  ngOnInit(): void {
  }

  public onPositionClick(): void {
    this._curPos = this.mapService.getPosition();
    this.stringPos = this._curPos.lat + ", " + this._curPos.lng;
  }

  public onSubmitKill(registerBite: NgForm): void {
    const biteCode: string = registerBite.value.biteCode;
    const position = this._curPos;

    const description: string = registerBite.value.description;
    if(position && description) {
      this.userService.kill(biteCode.toUpperCase(), description, this._curPos!.lat, this._curPos!.lng);
    }
    else if(position && !description) {
      this.userService.kill(biteCode.toUpperCase(), undefined, this._curPos!.lat, this._curPos!.lng);
    }
    else if(description && !position) {
      this.userService.kill(biteCode.toUpperCase(), description);
    }
    else {
      this.userService.kill(biteCode.toUpperCase());
    }
    this._curPos = undefined;
    registerBite.resetForm();
  }
}
