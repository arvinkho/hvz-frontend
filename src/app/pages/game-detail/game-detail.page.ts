import { Component, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbNavConfig } from '@ng-bootstrap/ng-bootstrap';
import { GameDto } from 'src/app/models/game-dto';
import { GameService } from 'src/app/services/game.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.page.html',
  styleUrls: ['./game-detail.page.css'],
  providers: [NgbNavConfig]
})
export class GameDetailPage implements OnInit {

  public currentWindowWidth: number;
  active = 1;

  get game(): GameDto {
    return this.gameService.singleGame;
  }

  

  constructor(
    private route: ActivatedRoute,
    private gameService: GameService,
    public userService: UserService,
    private router: Router,
    config: NgbNavConfig) {
    config.destroyOnHide = false;
  }

  onClick(){
    this.router.navigateByUrl('/map');
  }

  ngOnInit(): void {
    const gameId: string = this.route.snapshot.paramMap.get("game_id") ?? "";
    this.currentWindowWidth = window.innerWidth;
    this.gameService.getSingleGame(parseInt(gameId));
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth
  }

  onTabClick(): void {
    this.userService.updateStates();
  }

}
