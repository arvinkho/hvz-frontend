import { Component, Input, OnInit } from '@angular/core';
import { PlayerDto } from 'src/app/models/player-dto';
import { PlayerService } from 'src/app/services/player.service';


@Component({
  selector: 'app-player-states-list-item',
  templateUrl: './player-states-list-item.component.html',
  styleUrls: ['./player-states-list-item.component.css']
})
export class PlayerStatesListItemComponent implements OnInit {

  @Input() player?: PlayerDto;

  constructor(
    private playerService: PlayerService
  ) {}

  ngOnInit(): void {}

  onChangeClick(): void {
    this.player!.human = !this.player?.human;
    this.playerService.updatePlayer(this.player!.gameId, this.player!.playerId, this.player!.human);
  }

}
