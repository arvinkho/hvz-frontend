import { Injectable } from '@angular/core';
import {LatLng, latLng } from 'leaflet';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  private _latLng: LatLng = new LatLng(0,0);

  constructor() { 
    this.getPosition();
  }

  getCurrentPosition(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resp => {
        resolve({lat: resp.coords.latitude, lng: resp.coords.longitude});
      }, 
      err => {
        reject(err);
      });
    });
  }

  
  getPosition(): LatLng{
    navigator.geolocation.getCurrentPosition(pos => {
      this._latLng.lat = pos.coords.latitude
      this._latLng.lng = pos.coords.longitude
    })
    
    return this._latLng;
  }

  getMyPosition(){
    var options = {
      enableHighAccuracy: true,
      timeOut: 5000,
      maximumAge: 0
    }

    function success(pos: { coords: any; }){
      var crd = pos.coords;
    }

    function error(err: { code: any; message: any; }){
      console.warn(`ERROR(${err.code}): ${err.message}`);
    }
    let coords;
    coords = navigator.geolocation.getCurrentPosition(success, error, options)
    
  }



  public watchMyPosition(onChange: (arg0: { lat: number; lng: number; }) => void) {
    navigator.geolocation.watchPosition((response) => {
      onChange({
        lat: response.coords.latitude,
        lng: response.coords.longitude
      });
    })
  }
  /*
  // Usage
  service.watchPosition((coords) => {
     console.log(coords.lat, coords.lng);
  })
  */
  
 watchPostion(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.watchPosition(resp => {
        resolve({lat: resp.coords.latitude, lng: resp.coords.longitude});
      }, 
      err => {
        reject(err);
      });
    });
  }
}
