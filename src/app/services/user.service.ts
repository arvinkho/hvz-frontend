import { HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
import { AuthService, User } from "@auth0/auth0-angular";
import { LatLng } from "leaflet";
import { ToastrService } from "ngx-toastr";
import { BehaviorSubject, Observable, take, takeUntil, takeWhile } from "rxjs";
import { GameDto } from "../models/game-dto";
import { KillCreateDto } from "../models/kill-create-dto";
import { KillDto } from "../models/kill-dto";
import { PlayerCreateDto } from "../models/player-create-dto";
import { PlayerDto } from "../models/player-dto";
import { SquadCheckinCreateDto } from "../models/squad-checkin-create-dto";
import { SquadCreateDto } from "../models/squad-create-dto";
import { SquadDto } from "../models/squad-dto";
import { SquadMemberCreateDto } from "../models/squad-member-create-dto";
import { UserCreateDto } from "../models/user-create-dto";
import { UserDto } from "../models/user-dto";
import { UserModel } from "../models/user-model";
import { UserAPI } from "./api/user.api";
import { ErrorService } from "./error.service";
import { GameService } from "./game.service";
import { KillService } from "./kill.service";
import { LoadingService } from "./loading.service";
import { MapService } from "./mapApi/map.service";
import { PlayerService } from "./player.service";
import { SquadService } from "./squad.service";

@Injectable({
	providedIn: "root",
})
export class UserService {
	private _authUser: User | null | undefined;
	private _isAdmin: boolean = false;
	private _permissions: string[] = [];
	private _userUpdated: boolean = false;
	private _userSubject: BehaviorSubject<UserModel> = new BehaviorSubject<UserModel>(new UserModel());

	get user$(): Observable<UserModel> {
		if (!this._userUpdated && this.auth.isAuthenticated$) {
			this.updateUserFromAuth();
			this._userUpdated = true;
		}
		return this._userSubject.asObservable();
	}

	get game$(): Observable<GameDto | null | undefined> {
		return this.gameService.singleGame$;
	}

	get player$(): Observable<PlayerDto | null | undefined> {
		return this.playerService.singlePlayer$;
	}

	get squad$(): Observable<SquadDto | null | undefined> {
		return this.squadService.singleSquad$;
	}

	get kills$(): Observable<KillDto[]> {
		return this.killService.allKills$;
	}

	get loading$(): Observable<boolean> {
		return this.loadingService.loading$;
	}

	get isAdmin(): boolean {
		return this._isAdmin;
	}

	get permissions(): string[] {
		return this._permissions;
	}

	constructor(
		private auth: AuthService,
		private userAPI: UserAPI,
		private gameService: GameService,
		private playerService: PlayerService,
		private killService: KillService,
		private squadService: SquadService,
		private loadingService: LoadingService,
		private errorService: ErrorService,
		private mapService: MapService
	) {
		this.updateAuth();
		this.statePolling();
	}

	private statePolling(): void {
		setInterval(() => {
			this.updateStates();
		}, 1000 * 60);
	}

	public updateStates(): void {
		this.loadingService.loading$.pipe(takeWhile( loading => this.loadingService.loading == true))
			.subscribe({
				complete: () => {
					if (this.gameService.singleGame) {
						let gameId = this.gameService.singleGame.gameId;
						this.gameService.getSingleGame(gameId);
						this.squadService.getAllSquads(gameId);
						this.killService.getAllKills(gameId)
					}
					if (this.playerService.singlePlayer) {
						this.playerService.getSinglePlayer(this.playerService.singlePlayer.playerId);
					}
					if (this.squadService.singleSquad) {
						this.squadService.getSingleSquad(this.squadService.singleSquad.squadId, this.squadService.singleSquad.gameId);
					}
					this.gameService.getAllGames();
				}
			})
	}

	/**
	 * Get user from API. Checks if the user exists, and creates
	 * a new user in the backend if it doesn't.
	 */
	public getUser(): void {
		let userExists = false;
		if (this._authUser) {
			this.loadingService.startLoading();
			this.userAPI
				.getSingleUser(this._authUser.sub!.substring(
					this._authUser.sub!.indexOf("|") + 1
				))
				.subscribe({
					next: (user: UserDto | null) => {
						if (user) {
							userExists = true;
							let updatedUser = this._userSubject.getValue();
							// if (user.gameId) {
							// 	updatedUser.setGameId(user.gameId);
							// 	this.getGame(user.gameId);
							// 	this._userSubject.next(updatedUser);
							if (user.playerId) {
								updatedUser.setPlayerId(user.playerId);
								this.getPlayer(user.playerId);
								this._userSubject.next(updatedUser);
								// }
							} else {
								this.loadingService.stopLoading();
							}
						}
					},
					error: (error: HttpErrorResponse) => {
						this.errorService.setError(error.message);
					},
					complete: () => {
						this.loadingService.stopLoading();
					}
				});

			if (!userExists) {
				this.loadingService.startLoading();
				let newUser: UserCreateDto = new UserCreateDto(
					this._authUser?.name!,
					this._authUser?.email!
				);
				this.userAPI.createUser(newUser).subscribe({
					error: (error: HttpErrorResponse) => {
						this.errorService.setError(error.message);
						this.loadingService.stopLoading();
					},
					complete: () => {
						this.loadingService.stopLoading();
					},
				});
			}
		}
	}

	/**
	 * Updates the auth user from Auth0
	 */
	private updateAuth(): void {
		this.loadingService.startLoading();
		this.auth.getAccessTokenSilently(take(1)).subscribe({
			next: (token) => {
				let helper = new JwtHelperService();
				let decodedToken = helper.decodeToken(token);
				this._permissions = decodedToken.permissions;
				this._isAdmin = this._permissions.includes("admin");
				sessionStorage.setItem("isAdmin", this._isAdmin.toString());
			},
		})
		this.auth.user$.pipe(take(1)).subscribe({
			next: (authUser) => {
				this._authUser = authUser;
			},
			error: (error: HttpErrorResponse) => {
				this.errorService.setError(error.message);
				this.loadingService.stopLoading();
			},
			complete: () => {
				this.loadingService.stopLoading();
				this.updateUserFromAuth();
			},
		});
	}

	/**
	 * Synchronizes the current user model with the current auth user
	 */
	private updateUserFromAuth(): void {
		let updatedUser: UserModel = this._userSubject.getValue();
		if (this._authUser?.name) {
			updatedUser.setName(this._authUser.name);
		}
		if (this._authUser?.email) {
			updatedUser.setEmail(this._authUser.email);
		}
		if (this._authUser?.sub) {
			//updatedUser.setUserId(this._authUser.sub.replace("auth0|", ""));
			updatedUser.setUserId(
				this._authUser.sub.substring(
					this._authUser.sub.indexOf("|") + 1
				)
			);
		}
		if (this._authUser?.picture) {
			updatedUser.setAvatarUrl(this._authUser.picture);
		}
		this._userSubject.next(updatedUser);
		this.getUser();
	}

	private updatePlayer(playerId: number): void {
		this.loadingService.loading$.pipe(takeWhile( loading => this.loadingService.loading == true))
		.subscribe({
			complete: () => {
				this.playerService.getSinglePlayer(playerId);
			  }
		})
	}

	private updateSquads(gameId: number): void {
		this.loadingService.loading$.pipe(takeWhile( loading => this.loadingService.loading == true))
		.subscribe({
			complete: () => {
				this.squadService.getAllSquads(gameId);
			  }
		})
	}

	/**
	 * Selects a game as the current game for the user
	 * @param gameId gameId to select
	 */
	public getGame(gameId: number): void {
		let updatedUser = this._userSubject.getValue();
		updatedUser.setGameId(gameId);
		this._userSubject.next(updatedUser);
		this.gameService.getSingleGame(gameId);
	}

	/**
	 * MIGHT NOT BE USED
	 * @param gameId
	 */
	public setGame(gameId: number): void {
		console.log("setting game");

		this.loadingService.startLoading();
		this.userAPI
			.setGame(gameId)
			.pipe(take(1))
			.subscribe({
				next: (userDto: UserDto) => {
					let updatedUser = this._userSubject.getValue();
					updatedUser.setGameId(gameId);
					this._userSubject.next(updatedUser);
				},
				error: (error: HttpErrorResponse) => {
					this.errorService.setError(error.message);
					this.loadingService.stopLoading();
				},
				complete: () => {
					this.loadingService.stopLoading();
					this.setPlayer();
					this.getGame(gameId);
				},
			});
	}

	public setPlayer(): void {
		let gameId = this._userSubject.getValue().gameId;
		if (gameId) {
			console.log("No player found");
			let playerCreateDto: PlayerCreateDto = new PlayerCreateDto(
				true,
				this._userSubject.getValue().name!,
				this._userSubject.getValue().userId!,
				gameId
			);
			this.playerService.createPlayer(gameId, playerCreateDto);
			this.playerService.singlePlayer$
				.pipe(takeUntil(this.playerService.singlePlayer$))
				.subscribe({
					next: (newPlayer) => {
						let updatedUser = this._userSubject.getValue();
						updatedUser.setPlayerId(newPlayer?.playerId!);
					},
				});
		}
	}

	/**
	 * Sets a player for the current user. If a player already exists in the game, return the player.
	 * If no player exists, create a new player.
	 */
	public getPlayer(playerId: number): void {
		this.playerService.getSinglePlayer(playerId);
		if (!this._userSubject.getValue().gameId) {
			this.playerService.singlePlayer$
				.pipe(takeWhile((newPlayer) => newPlayer !== undefined))
				.subscribe({
					next: (newPlayer) => {
						if (newPlayer) {
							let updatedUser = this._userSubject.getValue();
							let gameId = newPlayer.gameId;
							let squadMemberId = newPlayer.squadMemberId;
							this.getGame(gameId);
							updatedUser.setGameId(gameId);
							this._userSubject.next(updatedUser);
							if (squadMemberId) {
								this.squadService.getSingleSquadBySquadMember(squadMemberId, gameId);
							}
						}
					},
					error: (error: HttpErrorResponse) => {
						this.errorService.setError(error.message);
						this.loadingService.stopLoading();
					},
					complete: () => {
						this.loadingService.stopLoading();
					},
				});
		}
	}

	/**
	 * Creates a kill and updates the DB
	 * @param biteCode unique identifier to kill a specific person
	 * @param story optional story to describe the kill
	 * @param latitude latitude position
	 * @param longitude longitude position
	 * @returns true if successful, false if not
	 */
	public kill(biteCode: string, story?: string, latitude?: number, longitude?: number): boolean {
		let currentGame: GameDto | null | undefined;
		let currentPlayer: PlayerDto | null | undefined;
		this.gameService.singleGame$.pipe(take(1)).subscribe({
			next: (game) => {
				currentGame = game;
			}
		});
		this.playerService.singlePlayer$.pipe(take(1)).subscribe({
			next: (player) => {
				currentPlayer = player;
			}
		});
		if (currentGame && currentPlayer) {
			if ((currentGame.gameState === "IN_PROGRESS") && !currentPlayer.human) {
				let createdKill: KillCreateDto = new KillCreateDto(biteCode, currentPlayer.playerId, currentGame.gameId, story, latitude, longitude);
				this.killService.createKill(currentGame.gameId, createdKill);
				
				return true;
			}
		}
		return false;
	}

	/**
	 * Creates a new squad
	 * @param squadName 
	 * @returns 
	 */
	public createSquad(squadName: string): boolean {
		let currentGame: GameDto | null | undefined;
		let currentPlayer: PlayerDto | null | undefined;
		this.gameService.singleGame$.pipe(take(1)).subscribe({
			next: (game) => {
				currentGame = game;
			},
		});
		this.playerService.singlePlayer$.pipe(take(1)).subscribe({
			next: (player) => {
				currentPlayer = player;
			}
		});
		
		if (currentGame && currentPlayer && !currentPlayer.squadMemberId) {
			let newSquad = new SquadCreateDto(squadName, currentGame.gameId);
			this.squadService.createSquad(newSquad, newSquad.gameId);
			this.updatePlayer(currentPlayer.playerId);
			this.updateSquads(currentGame.gameId);
			return true;
		}

		if (currentPlayer) {
			this.updatePlayer(currentPlayer.playerId);
		}

		if (currentGame) {
			this.updateSquads(currentGame.gameId);
		}

		return false;
	}

	public joinSquad(squadId: number): boolean {
		let currentGame: GameDto | null | undefined;
		let currentPlayer: PlayerDto | null | undefined;
		this.gameService.singleGame$.pipe(take(1)).subscribe({
			next: (game) => {
				currentGame = game;
			},
		});
		this.playerService.singlePlayer$.pipe(take(1)).subscribe({
			next: (player) => {
				currentPlayer = player;
			}
		});

		if (currentGame && currentPlayer && !currentPlayer.squadMemberId) {
			let squadMember = new SquadMemberCreateDto(currentGame.gameId, squadId, currentPlayer.playerId);
			this.squadService.createSquadMember(squadMember, currentGame.gameId, squadId);
			this.updatePlayer(currentPlayer.playerId);
			this.updateSquads(currentGame.gameId);
			return true;
		}

		if (currentPlayer) {
			this.updatePlayer(currentPlayer.playerId);
		}

		if (currentGame) {
			this.updateSquads(currentGame.gameId);
		}

		return false;
	}

	public leaveSquad(): boolean {
		let currentGame: GameDto | null | undefined;
		let currentPlayer: PlayerDto | null | undefined;
		let currentSquad: SquadDto | null | undefined;
		this.gameService.singleGame$.pipe(take(1)).subscribe({
			next: (game) => {
				currentGame = game;
			}
		});
		this.playerService.singlePlayer$.pipe(take(1)).subscribe({
			next: (player) => {
				currentPlayer = player;
			}
		});
		this.squadService.singleSquad$.pipe(take(1)).subscribe({
			next: (squad) => {
				currentSquad = squad;
			}
		});
		
		if (currentPlayer && currentSquad && currentGame && currentPlayer.squadMemberId) {
			this.squadService.deleteSquadMember(currentSquad.squadId, currentGame.gameId, currentPlayer.squadMemberId);
			this.squadService.reset();
			this.updatePlayer(currentPlayer.playerId);
			this.updateSquads(currentGame.gameId);
			return true;
		}

		if (currentPlayer) {
			this.updatePlayer(currentPlayer.playerId);
		}

		if (currentGame) {
			this.updateSquads(currentGame.gameId);
		}

		return false;
	}

	public squadCheckin(): boolean {
		let currentGame: GameDto | null | undefined;
		let currentPlayer: PlayerDto | null | undefined;
		let currentSquad: SquadDto | null | undefined;
		this.gameService.singleGame$.pipe(take(1)).subscribe({
			next: (game) => {
				currentGame = game;
			}
		});
		this.playerService.singlePlayer$.pipe(take(1)).subscribe({
			next: (player) => {
				currentPlayer = player;
			}
		});
		this.squadService.singleSquad$.pipe(take(1)).subscribe({
			next: (squad) => {
				currentSquad = squad;
			}
		});

		if (currentPlayer && currentSquad && currentGame && currentPlayer.squadMemberId) {
			let latLng: LatLng = this.mapService.getPosition();
			console.log(latLng);
			
			let squadCheckin = new SquadCheckinCreateDto(latLng.lat, latLng.lng, currentGame.gameId, currentSquad.squadId,
				currentPlayer.squadMemberId);
			this.squadService.createSquadCheckin(currentSquad.squadId, currentGame.gameId, squadCheckin);
			return true;
		}
		return false;

	}

	/**
	 * Leave the current game the user has signed up for. It is only possible
	 * to leave games in the "registration" and "complete" phases.
	 * @returns true if successful, false if not
	 */
	public leaveGame(): boolean {
		let currentGame: GameDto | null | undefined;
		let currentPlayer: PlayerDto | null | undefined;
		this.gameService.singleGame$.pipe(take(1)).subscribe({
			next: (game) => {
				currentGame = game;
			},
		});
		this.playerService.singlePlayer$.pipe(take(1)).subscribe({
			next: (player) => {
				currentPlayer = player;
			},
		});
		if (currentGame && currentPlayer) {
			if (currentGame.gameState !== "IN_PROGRESS") {
				this.playerService.deletePlayer(
					currentGame.gameId,
					currentPlayer.playerId
				);
				let updatedUser = this._userSubject.getValue();
				updatedUser.setGameId(null);
				updatedUser.setPlayerId(null);
				this.playerService.reset();
				this.gameService.reset();
				this.squadService.reset();
				return true;
			}
		}
		return false;
	}
}
