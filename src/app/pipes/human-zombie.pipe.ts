import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'humanZombie'
})
export class HumanZombiePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return value ? 'Human' : 'Zombie';
  }

}
