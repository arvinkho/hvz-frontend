export class KillDto {
    killId: number;
    timeOfDeath: number;
    story: string;
    latitude: number;
    longitude: number;
    gameId: number;
    killerId: number;
    victimId: number;
    killerName: string;
    victimName: string;
    
    

    constructor(killId: number, timeOfDeath: number, story: string, latitude: number, longitude: number,
        gameId: number, killerId: number, victimId: number, killerName: string, victimName: string) {
        this.killId = killId;
        this.timeOfDeath = timeOfDeath;
        this.story = story;
        this.latitude = latitude;
        this.longitude = longitude;
        this.gameId = gameId;
        this.killerId = killerId;
        this.victimId = victimId;
        this.killerName = killerName;
        this.victimName = victimName;
    }


}